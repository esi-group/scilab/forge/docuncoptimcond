// Copyright (C) 2008-2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Plot the functions phi(alpha) = f(x + alpha * u), with u eigenvector

//
// rosenbrock --
//   Compute the Rosenbrock cost function.
// Arguments
//   x : the point where to compute the cost function
//   ind : the flag which tells if f or g is to compute
//   f : the value of the cost function
//   g : the value of the gradient of the cost function
//  The following protocol is used
//  * if ind=1, returns the value of f,
//  * if ind=2, returns the value of g,
//  * if ind=4, returns both f and g
// Reference 
//   "An Automatic Method for Finding the Greatest or Least Value of a Function"
//   H. H. Rosenbrock, 1960, The Computer Journal
//
// Notes:
//   Starting point is x0 = [-1.2 1.0]
//   f(x0)=   24.2
//   Minimum is xopt = [1 1]
//   f(x0)=   0.
//   
function [f, g, H] = rosenbrock ( x )
  A = x(2) - x(1)^2
  B = 1-x(1)
  fm(1) = 10*A
  fm(2) = B
  f = sum(fm.^2)
  g(1) = - 400*A*x(1) - 2*B
  g(2) = 200 * A
  H(1,1) = 1200*x(1)^2 - 400*x(2) + 2
  H(1,2) = -400*x(1)
  H(2,1) = H(1,2)
  H(2,2) = 200
endfunction


// The function f(x* + a * u1)
function fs = phi1 ( a , xopt )
  [ fopt , gopt , Hopt ] = rosenbrock ( xopt )
  [R,D] = spec(Hopt)
  for i = 1 : size(a,"c")
    xs = xopt + a(i) * R(:,1)
    fs(i) = rosenbrock ( xs )
  end
endfunction

// The quadratic approximation
function fs = phi1_approx ( a , xopt )
  [ fopt , gopt , Hopt ] = rosenbrock ( xopt )
  [R,D] = spec(Hopt)
  for i = 1 : size(a,"c")
    fs(i) = fopt + 0.5 * D(1,1) * a(i)^2
  end
endfunction

xopt = [1.0 1.0]';

a = linspace ( 0 , 1.e-1 , 100 );
scf();
y1 = phi1 ( a , xopt );
plot ( x , y1 , "b-" )
y1_app = phi1_approx ( a , xopt );
plot ( x , y1_app , "r-." )

legend ( [ "Actual" "Quadratic" ] )

// The function f(x* + a * u2)
function fs = phi2 ( a , xopt )
  [ fopt , gopt , Hopt ] = rosenbrock ( xopt )
  [R,D] = spec(Hopt)
  for i = 1 : size(a,"c")
    xs = xopt + a(i) * R(:,2)
    fs(i) = rosenbrock ( xs )
  end
endfunction

// The quadratic approximation
function fs = phi2_approx ( a , xopt )
  [ fopt , gopt , Hopt ] = rosenbrock ( xopt )
  [R,D] = spec(Hopt)
  for i = 1 : size(a,"c")
    fs(i) = fopt + 0.5 * D(2,2) * a(i)^2
  end
endfunction

scf();
y2 = phi2(a,xopt,R);
plot ( x , y2 )
y2_app = phi2_approx ( a , xopt );
plot ( x , y2_app , "r-." )

legend ( [ "Actual" "Quadratic" ] )




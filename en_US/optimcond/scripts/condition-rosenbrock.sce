// Copyright (C) 2008-2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Analyse the condition number of Rosenbrock's function

//
// rosenbrock --
//   Compute the Rosenbrock cost function.
// Arguments
//   x : the point where to compute the cost function
//   ind : the flag which tells if f or g is to compute
//   f : the value of the cost function
//   g : the value of the gradient of the cost function
//  The following protocol is used
//  * if ind=1, returns the value of f,
//  * if ind=2, returns the value of g,
//  * if ind=4, returns both f and g
// Reference 
//   "An Automatic Method for Finding the Greatest or Least Value of a Function"
//   H. H. Rosenbrock, 1960, The Computer Journal
//
// Notes:
//   Starting point is x0 = [-1.2 1.0]
//   f(x0)=   24.2
//   Minimum is xopt = [1 1]
//   f(x0)=   0.
//   
function [ f , g , H ] = rosenbrock ( x )
  f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
  g(1) = - 400. * ( x(2) - x(1)**2 ) * x(1) -2. * ( 1. - x(1) )
  g(2) = 200. * ( x(2) - x(1)**2 )
  H(1,1) = 1200 * x(1)**2 - 400 * x(2) + 2
  H(1,2) = -400 * x(1)
  H(2,1) = H(1,2)
  H(2,2) = 200
endfunction

function f = rosenbrockC ( x1 , x2 )
  f = rosenbrock ( [x1,x2]' )
endfunction

// Plot the contour at optimum
x = linspace ( 0.9 , 1.1 , 100 );
y = linspace ( 0.9 , 1.1 , 100 );
contour ( x , y , rosenbrockC , [0.1 1 2 4] )

xopt = [1.0 1.0]';
[ fopt , gopt , Hopt ] = rosenbrock ( xopt );
plot ( xopt(1),xopt(2) , "g+" )

// Get the eigenvectors, eigenvalues
[R,D] = spec(Hopt)

// Plot the eigenvectors R(:,1) and R(:,2)
// Scale the eigenvectors
sc = 1.e-1
plot ( xopt(1) + sc * R(1,1) , xopt(2) + sc * R(2,1) , "r*" )
plot ( xopt(2) + sc * R(1,2) , xopt(2) + sc * R(2,2) , "bo" )


// Set xt
t = 2.e-3;
xt = xopt + t * R(:,2);
[ft,gt] = rosenbrock ( xt );

// Find a such that f(xopt + s * u1) = 0.0005004423272545233158
// where 0.0005004423272545233158 = f(xopt + u2)
function fs = phizero ( s , ft , R )
  xs = xopt + s * R(:,1)
  fs = rosenbrock ( xs ) - ft
endfunction

s = fsolve ( 1 , list ( phizero , ft , R ) )
// Compare to the theory for a quadratic function
t * sqrt(cond(Hopt))

xs = xopt + s * R(:,1)
[fs,gs] = rosenbrock ( xs )

// Plot the points
plot(xs(1),xs(2),"b*")
plot(xt(1),xt(2),"ro")

// Compare the norms of the distances to the optimum
[norm(xs - xopt) norm(xt - xopt)]
// Compare the norms of the gradients
[norm(gs) norm(gt)]

sqrt(cond(Hopt))

